<!DOCTYPE html>
<!--
================================================================================
 _  __     _     _ _    ____  _          _
| |/ _   _| |__ (_| | _|  _ \(___  _____| |
| ' | | | | '_ \| | |/ | |_) | \ \/ / _ | |
| . | |_| | |_) | |   <|  __/| |>  |  __| |
|_|\_\__,_|_.__/|_|_|\_|_|   |_/_/\_\___|_|

================================================================================
[Verifying my OpenPGP key: openpgp4fpr:e8923e0f9c7c84a663a37f850be1a9ae16f417a2]
================================================================================
You can hire me for a job in Switzerland or remote, you can see my contact on
this website.

My skills are:
- PHP, JavaScript, CSS, HTML
- WebGL, Blender, AutoCAD, FreeCAD, BIMserver
- Python, Lisp, Shell, Rust
- MySQL, PostgreSQL
- VB Script (yes I know...)
- Linux (Ubuntu, Debian, Manjaro) admin
================================================================================
-->
<html lang="en">
  <head>
		<title>KubikPixel - Profile and Contact</title>
		<meta charset="UTF-8">
    <meta name="author" content="KubikPixel">
    <meta name="keywords" content="portfolio, kubikpixel, contact, blog, mastodon, email, jabber, matrix, pgp, pgp key, social media">
    <meta name="description" content="Portfolio and contact site from KubikPixel. You find here my Blog, PGP keys and verified social media accounts.">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="google-site-verification" content="K_vnwLzHKnys18riiUXabt_md6pDe01rlEKK5WacBd0">
    <link rel="canonical" href="https://thunix.net/~kubikpixel/">
    <link rel=”alternate” hreflang=”en-GB” href=”https://thunix.net/~kubikpixel/index.php”>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Open Graph -->
    <meta property="og:title" content="KubikPixel - Profile and Contact">
    <meta property="og:site_name" content="KubikPixel">
    <meta property="og:url" content="https://thunix.net/~kubikpixel/">
    <meta property="og:locale" content="de_DE">
    <meta property="og:locale:alternate" content="de_AT">
    <meta property="og:locale:alternate" content="de_CH">
    <meta property="og:locale:alternate" content="en_GB">
    <meta property="og:locale:alternate" content="en_US">
    <meta property="og:description" content="Portfolio and contact site from KubikPixel. You find here my PGP keys and verified social media accounts.">
    <meta property="og:type" content="website">
    <meta property="og:type:website" content="https://thunix.net/~kubikpixel/">
    <meta property="og:image" content="https://thunix.net/~kubikpixel/assets/img/ogava.jpeg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="627">
    <meta property="og:image:alt" content="KubikPixel Avatar: Safety pin on red fabric">
    <!-- Style and Fonts -->
    <link type="text/css" rel="stylesheet" href="assets/css/mini-nord.min.css" media="all">
		<link type="text/css" rel="stylesheet" href="assets/css/style.css" media="all">
    <link type="text/css" rel="stylesheet" href="assets/css/line-awesome.min.css" media="all">
    <link type="text/css" rel="stylesheet" href="assets/css/toastify.css">

    <script type="text/javascript" src="assets/js/three.min.js"></script>
    <script type="text/javascript" src="assets/js/WebGL.js"></script>
  </head>
	<body>

    <canvas id="bg"></canvas>
    <script type="text/javascript">
      /* import * as THREE from './assets/js/three.min.js'; */
      /* import * as WEBGL from './assets/js/WebGL.js'; */

      function animate() {
        requestAnimationFrame(animate);

        octahedron.rotation.x += 0.002;
        octahedron.rotation.y += 0.004;
        octahedron.rotation.z += 0.001;

        icosahedron.rotation.x -= 0.002;
        icosahedron.rotation.y -= 0.004;
        icosahedron.rotation.z -= 0.001;

        renderer.render(scene, camera);
      }

      const scene = new THREE.Scene();
      const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
      const renderer = new THREE.WebGLRenderer({
        canvas: document.querySelector('#bg'),
        alpha: true,
      });

      renderer.setPixelRatio(window.devicePixelRatio);
      renderer.setSize(window.innerWidth, window.innerHeight);
      camera.position.setZ(80);

      var radius = window.innerHeight / 10;

      const materialb = new THREE.MeshBasicMaterial({
        color: 0x2698e6,
        wireframe: true,
      });
      const materialw = new THREE.MeshBasicMaterial({
        color: 0x7db6a7,
        wireframe: true,
      });

      const geometry00 = new THREE.OctahedronGeometry(radius);
      const octahedron = new THREE.Mesh(geometry00, materialw);
      const geometry01 = new THREE.IcosahedronGeometry(radius / 1.5);
      const icosahedron = new THREE.Mesh(geometry01, materialb);
      scene.add(octahedron, icosahedron);

      /* if (WEBGL.isWebGLAvailable()) { */
        animate();
      /* } else { */
      /*   const warning = WEBGL.getWebGLErrorMessage(); */
      /*   document.getElementById('bg').appendChild(warning); */
      /* } */

    </script>

		<header>
   		 <h1 class="section">KubikPixel</h1>
        <label for="modal-control">
           <i class="las la-share" title="Share"></i>
        </label>
		</header>

    <!-- Share -->
    <input type="checkbox" id="modal-control" class="modal">
    <div role="dialog" aria-labelledby="dialog-title">
      <div class="card shadowed">
        <h2 class="section" id="dialog-title">Share</h2>
        <label for="modal-control" class="modal-close"></label>
			  <img class="section media" src="assets/img/qrcode.png" alt="QR-Code with link to this site">
        <p class="section">
          <a href="https://twitter.com/intent/tweet?url=https://thunix.net/~kubikpixel/&text=KubikPixel&via=KubikPixel%20-%20Profile%20and%20Contact" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Twitter">
            <i class="lab la-twitter"></i>
            </span>
          </a>
          <a href="https://www.facebook.com/sharer.php?u=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Facebook">
            <i class="lab la-facebook"></i>
            </span>
          </a>
          <a href="https://reddit.com/submit?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Reddit">
            <i class="lab la-reddit"></i>
            </span>
          </a>
          <a href="https://vk.com/share.php?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="VK">
            <i class="lab la-vk"></i>
            </span>
          </a>
          <a href="https://share.diasporafoundation.org/?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Diaspora">
            <i class="lab la-diaspora"></i>
            </span>
          </a>
          <a href="https://www.linkedin.com/shareArticle?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="LinkedIn">
            <i class="lab la-linkedin-in"></i>
            </span>
          </a>
          <a href="https://www.xing.com/app/user?op=share&url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Xing">
            <i class="lab la-xing"></i>
            </span>
          </a>
          <a href="mailto:%7Bemail_address%7D?subject=KubikPixel&body=https://thunix.net/~kubikpixel/%20KubikPixel%20-%20Profile%20and%20Contact" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Email">
            <i class="las la-at"></i>
            </span>
          </a>
        </p>
        <p class="section">
          <a href="https://mail.google.com/mail/?view=cm&to=%7Bemail_address%7D&su=KubikPixel&body=https://thunix.net/~kubikpixel/&bcc=%7Bemail_address%7D&cc=%7Bemail_address%7D" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Gmail">
            <i class="las la-envelope"></i>
            </span>
          </a>
          <a href="https://compose.mail.yahoo.com/?body=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Yahoo">
            <i class="lab la-yahoo"></i>
            </span>
          </a>
          <a href="whatsapp://send?text=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="WhatsApp">
            <i class="lab la-whatsapp"></i>
            </span>
          </a>
          <a href="https://telegram.me/share/url?url=https://thunix.net/~kubikpixel/&text=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Telegram">
            <i class="lab la-telegram"></i>
            </span>
          </a>
          <a href="https://web.skype.com/share?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Skype">
            <i class="lab la-skype"></i>
            </span>
          </a>
          <a
            href="sms:%7Bphone_number%7D?body=KubikPixel%20-%20Profile%20and%20Contact%20https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="SMS">
            <i class="las la-sms"></i>
            </span>
          </a>
          <a href="https://www.evernote.com/clip.action?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Evernote">
            <i class="lab la-evernote"></i>
            </span>
          </a>
          <a href="https://getpocket.com/save?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Pocket">
            <i class="lab la-get-pocket"></i>
            </span>
          </a>
        </p>
      </div>
    </div>

    <main>
    <article>
		<div class="row">

      <!-- Download -->

      <blockquote class="shadowed">
        <h2>Downloads</h2>
        Here I provide my <strong>assets</strong> and <strong>scripts</strong> for 2D &amp; 3D artists. Most of it is for the <strong>Godot</strong> engine and/or the <strong>Blender</strong> and mostly free. If you find it useful and want to support me you can <strong>donate</strong> an amount of your discretion. The links lead directly to the download pages of the respective platforms.
      </blockquote>

      <section>
        <iframe class="dlink" src="https://itch.io/embed/1188517?bg_color=defed6&amp;fg_color=222222&amp;link_color=c57e0e&amp;border_color=56b763" width="507" height="167" frameborder="0"><a href="https://saarta.itch.io/yalwa">YALFA by Saarta</a></iframe>
        <iframe class="dlink" src="https://itch.io/embed/1189954?bg_color=000000&amp;fg_color=ffffff&amp;link_color=5afad4&amp;border_color=6e86a0" width="507" height="167" frameborder="0"><a href="https://saarta.itch.io/scifi-hologram">SciFi Hologram [Godot] by Saarta</a></iframe>
        <div class="dlink" style="background-color:#fff;color:#293c5c;">
          <img src="assets/img/openclip.svg" alt="openclipart.org">
          <h2>Weather Symbols</h2>
          <p>OpenClipart - free &amp; libre SVG vector weather graphics</p>
          <a rel="external" href="https://openclipart.org/artist/spite">Download Now</a>
        </div>
      </section>

    </div>
    </article>
		</main>
      <h3><a rel="me" href="index.php"><i class="las la-home"></i> Home</a> | <a rel="me" href="articles.php"><i class="las la-newspaper"></i> Articles</a></h3>
      <hr>
		<footer>
		 	<p>&copy; &amp; &lt;&check;&gt; with &hearts; 2021 by KubikPixel</p>
  </body>
</html>
