<!DOCTYPE html>
<!--
================================================================================
 _  __     _     _ _    ____  _          _
| |/ _   _| |__ (_| | _|  _ \(___  _____| |
| ' | | | | '_ \| | |/ | |_) | \ \/ / _ | |
| . | |_| | |_) | |   <|  __/| |>  |  __| |
|_|\_\__,_|_.__/|_|_|\_|_|   |_/_/\_\___|_|

================================================================================
[Verifying my OpenPGP key: openpgp4fpr:e8923e0f9c7c84a663a37f850be1a9ae16f417a2]
================================================================================
You can hire me for a job in Switzerland or remote, you can see my contact on
this website.

My skills are:
- PHP, JavaScript, CSS, HTML
- WebGL, Blender, AutoCAD, FreeCAD, BIMserver
- Python, Lisp, Shell, Rust
- MySQL, PostgreSQL
- VB Script (yes I know...)
- Linux (Ubuntu, Debian, Manjaro) admin
================================================================================
-->
<html lang="en">
  <head>
		<title>KubikPixel - Profile and Contact</title>
		<meta charset="UTF-8">
    <meta name="author" content="KubikPixel">
    <meta name="keywords" content="portfolio, kubikpixel, contact, blog, mastodon, email, jabber, matrix, pgp, pgp key, social media">
    <meta name="description" content="Portfolio and contact site from KubikPixel. You find here my Blog, PGP keys and verified social media accounts.">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="google-site-verification" content="K_vnwLzHKnys18riiUXabt_md6pDe01rlEKK5WacBd0">
    <link rel="canonical" href="https://thunix.net/~kubikpixel/">
    <link rel=”alternate” hreflang=”en-GB” href=”https://thunix.net/~kubikpixel/index.php”>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="me" href="https://github.com/KubikPixel">
    <!-- Open Graph -->
    <meta property="og:title" content="KubikPixel - Profile and Contact">
    <meta property="og:site_name" content="KubikPixel">
    <meta property="og:url" content="https://thunix.net/~kubikpixel/">
    <meta property="og:locale" content="de_DE">
    <meta property="og:locale:alternate" content="de_AT">
    <meta property="og:locale:alternate" content="de_CH">
    <meta property="og:locale:alternate" content="en_GB">
    <meta property="og:locale:alternate" content="en_US">
    <meta property="og:description" content="Portfolio and contact site from KubikPixel. You find here my PGP keys and verified social media accounts.">
    <meta property="og:type" content="website">
    <meta property="og:type:website" content="https://thunix.net/~kubikpixel/">
    <meta property="og:image" content="https://thunix.net/~kubikpixel/assets/img/ogava.jpeg">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="627">
    <meta property="og:image:alt" content="KubikPixel Avatar: Safety pin on red fabric">
    <!-- Style and Fonts -->
    <link type="text/css" rel="stylesheet" href="assets/css/mini-nord.min.css" media="all">
		<link type="text/css" rel="stylesheet" href="assets/css/style.css" media="all">
    <link type="text/css" rel="stylesheet" href="assets/css/line-awesome.min.css" media="all">
    <link type="text/css" rel="stylesheet" href="assets/css/toastify.css">

    <script type="text/javascript" src="assets/js/three.min.js"></script>
    <script type="text/javascript" src="assets/js/WebGL.js"></script>
  </head>
	<body>

    <canvas id="bg"></canvas>
    <script type="text/javascript">
      /* import * as THREE from './assets/js/three.min.js'; */
      /* import * as WEBGL from './assets/js/WebGL.js'; */

      function animate() {
        requestAnimationFrame(animate);

        octahedron.rotation.x += 0.002;
        octahedron.rotation.y += 0.004;
        octahedron.rotation.z += 0.001;

        icosahedron.rotation.x -= 0.002;
        icosahedron.rotation.y -= 0.004;
        icosahedron.rotation.z -= 0.001;

        renderer.render(scene, camera);
      }

      const scene = new THREE.Scene();
      const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
      const renderer = new THREE.WebGLRenderer({
        canvas: document.querySelector('#bg'),
        alpha: true,
      });

      renderer.setPixelRatio(window.devicePixelRatio);
      renderer.setSize(window.innerWidth, window.innerHeight);
      camera.position.setZ(80);

      var radius = window.innerHeight / 10;

      const materialb = new THREE.MeshBasicMaterial({
        color: 0x2698e6,
        wireframe: true,
      });
      const materialw = new THREE.MeshBasicMaterial({
        color: 0x7db6a7,
        wireframe: true,
      });

      const geometry00 = new THREE.OctahedronGeometry(radius);
      const octahedron = new THREE.Mesh(geometry00, materialw);
      const geometry01 = new THREE.IcosahedronGeometry(radius / 1.5);
      const icosahedron = new THREE.Mesh(geometry01, materialb);
      scene.add(octahedron, icosahedron);

      /* if (WEBGL.isWebGLAvailable()) { */
        animate();
      /* } else { */
      /*   const warning = WEBGL.getWebGLErrorMessage(); */
      /*   document.getElementById('bg').appendChild(warning); */
      /* } */

    </script>

		<header>
   		<h1 class="section">KubikPixel</h1>
      <span class="tooltip bottom" aria-label="Share my site">
        <label for="modal-control" title="Share my site">
          <i class="las la-share"></i>
        </label>
      </span>
		</header>

    <!-- Share -->
    <input type="checkbox" id="modal-control" class="modal">
    <div role="dialog" aria-labelledby="dialog-title">
      <div class="card shadowed">
        <h2 class="section" id="dialog-title">Share</h2>
        <label for="modal-control" class="modal-close"></label>
			  <img class="section media" src="assets/img/qrcode.png" alt="QR-Code with link to this site">
        <p class="section">
          <a href="https://twitter.com/intent/tweet?url=https://thunix.net/~kubikpixel/&text=KubikPixel&via=KubikPixel%20-%20Profile%20and%20Contact" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Twitter">
            <i class="lab la-twitter"></i>
            </span>
          </a>
          <a href="https://www.facebook.com/sharer.php?u=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Facebook">
            <i class="lab la-facebook"></i>
            </span>
          </a>
          <a href="https://reddit.com/submit?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Reddit">
            <i class="lab la-reddit"></i>
            </span>
          </a>
          <a href="https://vk.com/share.php?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="VK">
            <i class="lab la-vk"></i>
            </span>
          </a>
          <a href="https://share.diasporafoundation.org/?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Diaspora">
            <i class="lab la-diaspora"></i>
            </span>
          </a>
          <a href="https://www.linkedin.com/shareArticle?url=https://thunix.net/~kubikpixel/&title=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="LinkedIn">
            <i class="lab la-linkedin-in"></i>
            </span>
          </a>
          <a href="https://www.xing.com/app/user?op=share&url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Xing">
            <i class="lab la-xing"></i>
            </span>
          </a>
          <a href="mailto:%7Bemail_address%7D?subject=KubikPixel&body=https://thunix.net/~kubikpixel/%20KubikPixel%20-%20Profile%20and%20Contact" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Email">
            <i class="las la-at"></i>
            </span>
          </a>
        </p>
        <p class="section">
          <a href="https://mail.google.com/mail/?view=cm&to=%7Bemail_address%7D&su=KubikPixel&body=https://thunix.net/~kubikpixel/&bcc=%7Bemail_address%7D&cc=%7Bemail_address%7D" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Gmail">
            <i class="las la-envelope"></i>
            </span>
          </a>
          <a href="https://compose.mail.yahoo.com/?body=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Yahoo">
            <i class="lab la-yahoo"></i>
            </span>
          </a>
          <a href="whatsapp://send?text=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="WhatsApp">
            <i class="lab la-whatsapp"></i>
            </span>
          </a>
          <a href="https://telegram.me/share/url?url=https://thunix.net/~kubikpixel/&text=KubikPixel" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Telegram">
            <i class="lab la-telegram"></i>
            </span>
          </a>
          <a href="https://web.skype.com/share?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Skype">
            <i class="lab la-skype"></i>
            </span>
          </a>
          <a
            href="sms:%7Bphone_number%7D?body=KubikPixel%20-%20Profile%20and%20Contact%20https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="SMS">
            <i class="las la-sms"></i>
            </span>
          </a>
          <a href="https://www.evernote.com/clip.action?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Evernote">
            <i class="lab la-evernote"></i>
            </span>
          </a>
          <a href="https://getpocket.com/save?url=https://thunix.net/~kubikpixel/" target="_blank" rel="nofollow">
            <span class="tooltip" aria-label="Pocket">
            <i class="lab la-get-pocket"></i>
            </span>
          </a>
        </p>
      </div>
    </div>

    <main>
    <article>
		<div class="h-card row">

    <!-- Avatar -->
		<section class="card shadowed">
			<h2 class="section">Avatar</h2>
      <img class="u-photo section dark circular" src="assets/img/avatar.jpeg" alt="KubikPixel Avatar: Safety pin on red fabric">
		</section>

    <!-- About -->
		<section class="card shadowed">
			<h2 class="section">About</h2>
			<p class="p-note section about">
			I'm a Web &amp; App <mark class="tertiary tag">developer</mark> with
      <mark class="tertiary tag">3D</mark> experience and a Geek with a Punk
      attitude. I'm a <mark class="tertiary tag">open source</mark> evangelist
      and a <a href="https://1x.engineer/" target="_blank" rel="nofollow">1x
      Engineer</a> who uses Linux (Debian, Manjaro) on a daily basis.
      You can contact me in either English or German for a complete CV.
			</p>
		</section>

    <!-- Coding -->
		<section class="card shadowed">
			<h2 class="section">IT &amp; Coding</h2>
			<ul class="section">
        <li><i class="las la-hand-pointer"></i> PHP, JS, CSS, HTML</li>
        <li><i class="las la-code"></i> <mark class="tertiary tag">Python</mark>, Rust, Lisp, Shell</li>
        <li><i class="las la-database"></i> MySQL, <mark class="tertiary tag">MariaDB</mark>, PostgreSQL</li>
        <li><i class="las la-terminal"></i> <mark class="tertiary tag">Linux</mark>, Debian &amp; Manjaro</li>
        <li><i class="las la-file-code"></i> Git, Vim, SSH</li>
			</ul>
		</section>

    <!-- 3D Graphics -->
		<section class="card shadowed">
			<h2 class="section">3D &amp; Graphics</h2>
			<ul class="section">
        <li><i class="las la-cube"></i> <mark class="tertiary tag">WebGL</mark>, Blender, Godot</li>
        <li><i class="las la-drafting-compass"></i> AutoCAD, <mark class="tertiary tag">FreeCAD</mark>, BIMserver</li>
        <li><i class="las la-project-diagram"></i> BIM, <mark class="tertiary tag">CAM</mark>, PLM</li>
        <li><i class="las la-paint-brush"></i> Inkscape, Gimp, Krita</li>
			</ul>
		</section>

    <!-- Projects -->
		<section class="card shadowed">
			<h2 class="section">Projects</h2>
      <p class="section">
        The current 💻 projects I am working on in my spare time:
      </p>
			<ul class="section">
        <li><i class="las la-download"></i> <a href="downloads.php">Downloads</a>: 3D Assets &amp; Scripts</li>
        <li><i class="lab la-css3"></i> <a href="https://codeberg.org/KubikPixel/aclue">aclue</a>: A CSS icon color library</li>
        <li><i class="las la-code"></i> cURLcard: <code>curl -sL 0x0.st/NlpO</code></li>
<!-- <li><i class="las la-hand-point-right"></i> <a href="https://kubikpixel.github.io/pipeinstall/">Pipe Inslall</a>: A curated list of bad behavior</li> -->
			</ul>
		</section>

    <!-- Support -->
		<section class="card shadowed">
			<h2 class="section">Support</h2>
      <p class="section">
        If you would like to support me and my work, you can do so to spend my a ☕ through Liberapay, <a href="#cc">Crypto-Currency</a>  or a paid itch.io download.
      </p>
			<p class="section">
        <script src="https://liberapay.com/KubikPixel/widgets/button.js"></script>
        <noscript><a href="https://liberapay.com/KubikPixel/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
        <a rel="external" href="https://saarta.itch.io/" class="btn-itchio"><i class="lab la-itch-io"></i> itch.io</a>
 			</p>
		</section>

    <!-- Internet -->
		<section class="card shadowed">
			<h2 class="section">Internet</h2>
			<ul class="section">
				<li><i class="las la-rss"></i> <a href="https://paper.wf/kubikpixel/">Blog</a> (<a href="https://paper.wf/kubikpixel/feed/">Feed</a>)</li>
				<li><i class="lab la-mastodon"></i> <a rel="me" href="https://chaos.social/@kubikpixel">Mastodon</a></li>
				<li><i class="lab la-reddit"></i> <a href="https://www.reddit.com/user/KubikPixel">Reddit</a></li>
				<li><i class="lab la-git"></i> <a rel="me" href="https://codeberg.org/KubikPixel">Git repo</a></li>
				<li><i class="las la-rocket"></i> <a href="gemini://tilde.pink/~kubikpixel/">Gemini</a></li>
				<li><i class="las la-link"></i> <a class="u-url" rel="me author" href="https://thunix.net/~kubikpixel/">Website</a></li>
			</ul>
		</section>

    <!-- Contact -->
		<section class="card shadowed">
			<h2 class="section">Contact</h2>
			<ul class="section">
				<li><i class="las la-address-card"></i> <a href="https://qcard.link/card/#QkVHSU46VkNBUkQNCk46O0t1YmlrUGl4ZWw7OzsNCkZOOkt1YmlrUGl4ZWwNClRJVExFOldlYiBkZXZlbG9wZXINCkVNQUlMOmt1YmlrcGl4ZWxAZGlzbWFpbC5kZQ0KVVJMOmh0dHBzOi8vdGh1bml4Lm5ldC9+a3ViaWtwaXhlbC8NCk5PVEU6SSBidWlsZCAzRCB3ZWIgYXBwbGljYXRpb25zDQpYLUpBQkJFUjprdWJpa3BpeGVsQDUyMjIuZGUNCkVORDpWQ0FSRA==">vCard</a></li>
				<li><i class="las la-user-check"></i> <a href="https://keyoxide.org/e8923e0f9c7c84a663a37f850be1a9ae16f417a2">Verified Accounts</a></li>
				<li><i class="las la-envelope"></i> <a class="u-email" rel="me" href="mailto:kubikpixel@dismail.de">Email</a> (encrypt or <a href="https://delta.chat/">Delta Chat</a>)</li>
				<li><i class="las la-comments"></i> <a href="xmpp:kubikpixel@5222.de">Jabber / XMPP</a></li>
				<li><i class="las la-comment"></i> <a href="https://matrix.to/#/@kubikpixel:tchncs.de">Matrix</a></li>
        <li><i class="las la-cloud-upload-alt"></i> <a target="_blank" rel="noreferrer noopener" href="https://nextcloud.com/sharing#kubikpixel@dismail.de@sam.nl.tab.digital">Nextcloud</a></li>
 			</ul>
		</section>

    <!-- Social -->
		<section class="card shadowed">
			<h2 class="section">Fediverse</h2>
      <iframe class="section media" allowfullscreen sandbox="allow-top-navigation allow-scripts" width="322" height="200" src="https://www.mastofeed.com/apiv2/feed?userurl=https%3A%2F%2Fchaos.social%2Fusers%2Fkubikpixel&theme=light&size=100&header=false&replies=false&boosts=false"></iframe>
		</section>
    </div>
    <hr>
    <!-- PGP & SSh Key and Fingerprint -->
		<section>
        <blockquote class="shadowed">
          <h2>Encrypted Emails</h2>
          If you want to send me an <strong>email</strong>, then I ask you please to do this <strong>encrypted</strong>.<br>
          How this works and what you have to do for it and why I published my key here and what the fingerprint is you can read <a href="https://emailselfdefense.fsf.org/" target="_new" title="fsf.org">here...</a>
        </blockquote>
        <div>
          <h3><i class="las la-fingerprint"></i> PGP Fingerprint</h3>
          <span class="tooltip" aria-label="Copy PGP Fingerprint to clipboard">
            <button id="btn-fpr" class="small primary shadowed" type="button">
              <i class="las la-clipboard"></i>
            </button>
          </span>
        </div>
        <label for="pgp-fpr">GPG Key ID: 0BE1A9AE16F417A2</label>
        <input type="text" class="shadowed" id="pgp-fpr" readonly="readonly" value="E892 3E0F 9C7C 84A6 63A3 7F85 0BE1 A9AE 16F4 17A2">
        <div>
          <h3><i class="las la-key"></i> PGP Public Key</h3>
            <span class="tooltip" aria-label="Copy PGP Public Key to clipboard">
              <button id="btn-key" class="small primary shadowed" type="button">
                <i class="las la-clipboard"></i>
            </button>
            </span>
        </div>
      <label for="pgp-key">GPG Key ID: 0BE1A9AE16F417A2</label>
<textarea class="u-key shadowed" id="pgp-key" readonly="readonly">
-----BEGIN PGP PUBLIC KEY BLOCK-----
Comment: E892 3E0F 9C7C 84A6 63A3  7F85 0BE1 A9AE 16F4 17A2
Comment: KubikPixel <kubikpixel@dismail.de>

xjMEXz0j+RYJKwYBBAHaRw8BAQdAdrN+hX1MB/IRvfcocZmxbGkAPJ/uocyYpl6D
0tOYJG3NIkt1YmlrUGl4ZWwgPGt1YmlrcGl4ZWxAZGlzbWFpbC5kZT7CwpsEExYI
AwMCGwMFCQlmAYAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOj
f4UL4amuFvQXogUCYQZcDUoUgAAAAAASAC9wcm9vZkBtZXRhY29kZS5iaXpodHRw
czovL25ld3MueWNvbWJpbmF0b3IuY29tL3VzZXI/aWQ9S3ViaWtQaXhlbMAgFIAA
AAAAEgDFcHJvb2ZAbWV0YWNvZGUuYml6eG1wcDprdWJpa3BpeGVsQGRpc21haWwu
ZGU/b21lbW8tc2lkLTEyNjY3NTA4MD1lNmVmM2E1MDQ0YTAzZjgwYzBjNWU4MzU5
YzYyOTEzZTY0NmQ2ZjE5MTc1MmU4YWYwMmMwM2JlYWI3OTBjYTAzO29tZW1vLXNp
ZC0xNDg1NTAzNDg1PTFjNTI1MWM4YTEwODYyOTgxNzAzMmNkODA3NzEzMjRjNTcy
YjNkNmEzOWYzZGRjNmRkZjJjZjRlMGViN2NhN2FGFIAAAAAAEgArcHJvb2ZAbWV0
YWNvZGUuYml6aHR0cHM6Ly9jb2RlYmVyZy5vcmcvS3ViaWtQaXhlbC9naXRlYV9w
cm9vZnAUgAAAAAASAFVwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3d3dy5yZWRk
aXQuY29tL3VzZXIvS3ViaWtQaXhlbC9jb21tZW50cy9pcW1yMXMva2V5b3hpZGVf
b3BlbnBncF92ZXJpZmljYXRpb24vOxSAAAAAABIAIHByb29mQG1ldGFjb2RlLmJp
emh0dHBzOi8vY2hhb3Muc29jaWFsL0BrdWJpa3BpeGVsoxSAAAAAABIAiHByb29m
QG1ldGFjb2RlLmJpem1hdHJpeDp1L0BrdWJpa3BpeGVsOnRjaG5jcy5kZT9vcmcu
a2V5b3hpZGUucj0hZEJmUVp4Q29HVm1TVHVqZml2Om1hdHJpeC5vcmcmb3JnLmtl
eW94aWRlLmU9JFIxOWFDMTBpUUx4UEFVTGVYMnZUM0pmWDJOYTNxVUZVM1BFbnNW
M3NLN28ACgkQC+Gprhb0F6JZxwEAnoQg0I/mMWugWrDLHaSI0eNijJz5ELLvDNzj
6d7a/LkBAPjFRUD8rM1gHm82ZNQCPU2QFWK5RQLsTFgd3uhjwv0KwsLwBBMWCANY
AhsDBQkJZgGABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAFiEE6JI+D5x8hKZjo3+F
C+Gprhb0F6IFAmBLwQOjFIAAAAAAEgCIcHJvb2ZAbWV0YWNvZGUuYml6bWF0cml4
OnUvQGt1YmlrcGl4ZWw6dGNobmNzLmRlP29yZy5rZXlveGlkZS5yPSFkQmZRWnhD
b0dWbVNUdWpmaXY6bWF0cml4Lm9yZyZvcmcua2V5b3hpZGUuZT0kUjE5YUMxMGlR
THhQQVVMZVgydlQzSmZYMk5hM3FVRlUzUEVuc1Yzc0s3bzsUgAAAAAASACBwcm9v
ZkBtZXRhY29kZS5iaXpodHRwczovL2NoYW9zLnNvY2lhbC9Aa3ViaWtwaXhlbHAU
gAAAAAASAFVwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3d3dy5yZWRkaXQuY29t
L3VzZXIvS3ViaWtQaXhlbC9jb21tZW50cy9pcW1yMXMva2V5b3hpZGVfb3BlbnBn
cF92ZXJpZmljYXRpb24vVBSAAAAAABIAOXByb29mQG1ldGFjb2RlLmJpemh0dHBz
Oi8vdHdpdHRlci5jb20va3ViaWtwaXhlbC9zdGF0dXMvMTMwNDAwODAzMDA0OTIz
OTA0MUYUgAAAAAASACtwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL2NvZGViZXJn
Lm9yZy9LdWJpa1BpeGVsL2dpdGVhX3Byb29mwCAUgAAAAAASAMVwcm9vZkBtZXRh
Y29kZS5iaXp4bXBwOmt1YmlrcGl4ZWxAZGlzbWFpbC5kZT9vbWVtby1zaWQtMTI2
Njc1MDgwPWU2ZWYzYTUwNDRhMDNmODBjMGM1ZTgzNTljNjI5MTNlNjQ2ZDZmMTkx
NzUyZThhZjAyYzAzYmVhYjc5MGNhMDM7b21lbW8tc2lkLTE0ODU1MDM0ODU9MWM1
MjUxYzhhMTA4NjI5ODE3MDMyY2Q4MDc3MTMyNGM1NzJiM2Q2YTM5ZjNkZGM2ZGRm
MmNmNGUwZWI3Y2E3YUoUgAAAAAASAC9wcm9vZkBtZXRhY29kZS5iaXpodHRwczov
L25ld3MueWNvbWJpbmF0b3IuY29tL3VzZXI/aWQ9S3ViaWtQaXhlbAAKCRAL4amu
FvQXoj0FAQCZJ025qPN00G2jrjUseuX1dvdxJj5fo9o6SSCiY0GWEQD/QD5AIdKn
vGvUE+nTMPWKx5jvXzV3Hn9AkkOZiWaIbAXCwvAEExYIA1gCGwMFCQlmAYAFCwkI
BwIGFQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOjf4UL4amuFvQXogUCYEh8
SKMUgAAAAAASAIhwcm9vZkBtZXRhY29kZS5iaXptYXRyaXg6dS9Aa3ViaWtwaXhl
bDp0Y2huY3MuZGU/b3JnLmtleW94aWRlLnI9IWRCZlFaeENvR1ZtU1R1amZpdjpt
YXRyaXgub3JnJm9yZy5rZXlveGlkZS5lPSRSMTlhQzEwaVFMeFBBVUxlWDJ2VDNK
ZlgyTmEzcVVGVTNQRW5zVjNzSzdvOxSAAAAAABIAIHByb29mQG1ldGFjb2RlLmJp
emh0dHBzOi8vY2hhb3Muc29jaWFsL0BrdWJpa3BpeGVscBSAAAAAABIAVXByb29m
QG1ldGFjb2RlLmJpemh0dHBzOi8vd3d3LnJlZGRpdC5jb20vdXNlci9LdWJpa1Bp
eGVsL2NvbW1lbnRzL2lxbXIxcy9rZXlveGlkZV9vcGVucGdwX3ZlcmlmaWNhdGlv
bi9UFIAAAAAAEgA5cHJvb2ZAbWV0YWNvZGUuYml6aHR0cHM6Ly90d2l0dGVyLmNv
bS9rdWJpa3BpeGVsL3N0YXR1cy8xMzA0MDA4MDMwMDQ5MjM5MDQxRhSAAAAAABIA
K3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vY29kZWJlcmcub3JnL0t1YmlrUGl4
ZWwvZ2l0ZWFfcHJvb2bAIBSAAAAAABIAxXByb29mQG1ldGFjb2RlLmJpenhtcHA6
a3ViaWtwaXhlbEBkaXNtYWlsLmRlP29tZW1vLXNpZC0xMjY2NzUwODA9ZTZlZjNh
NTA0NGEwM2Y4MGMwYzVlODM1OWM2MjkxM2U2NDZkNmYxOTE3NTJlOGFmMDJjMDNi
ZWFiNzkwY2EwMztvbWVtby1zaWQtMTQ4NTUwMzQ4NT0xYzUyNTFjOGExMDg2Mjk4
MTcwMzJjZDgwNzcxMzI0YzU3MmIzZDZhMzlmM2RkYzZkZGYyY2Y0ZTBlYjdjYTdh
ShSAAAAAABIAL3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vbmV3cy55Y29tYmlu
YXRvci5jb20vdXNlcj9pZD1LdWJpa1BpeGVsAAoJEAvhqa4W9Bei6GkBAKRfjcL/
phl14GudiAiehPC8j7Wh8tB8vswsswwRs7OFAQDyTLUZp/+3vw2BsBjfchxFDTJ8
bQn/nEIiu1LWIYE0DMLCTAQTFggCtAIbAwUJCWYBgAULCQgHAgYVCgkICwIEFgID
AQIeAQIXgBYhBOiSPg+cfISmY6N/hQvhqa4W9BeiBQJgPBVSShSAAAAAABIAL3By
b29mQG1ldGFjb2RlLmJpemh0dHBzOi8vbmV3cy55Y29tYmluYXRvci5jb20vdXNl
cj9pZD1LdWJpa1BpeGVswCAUgAAAAAASAMVwcm9vZkBtZXRhY29kZS5iaXp4bXBw
Omt1YmlrcGl4ZWxAZGlzbWFpbC5kZT9vbWVtby1zaWQtMTI2Njc1MDgwPWU2ZWYz
YTUwNDRhMDNmODBjMGM1ZTgzNTljNjI5MTNlNjQ2ZDZmMTkxNzUyZThhZjAyYzAz
YmVhYjc5MGNhMDM7b21lbW8tc2lkLTE0ODU1MDM0ODU9MWM1MjUxYzhhMTA4NjI5
ODE3MDMyY2Q4MDc3MTMyNGM1NzJiM2Q2YTM5ZjNkZGM2ZGRmMmNmNGUwZWI3Y2E3
YUYUgAAAAAASACtwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL2NvZGViZXJnLm9y
Zy9LdWJpa1BpeGVsL2dpdGVhX3Byb29mVBSAAAAAABIAOXByb29mQG1ldGFjb2Rl
LmJpemh0dHBzOi8vdHdpdHRlci5jb20va3ViaWtwaXhlbC9zdGF0dXMvMTMwNDAw
ODAzMDA0OTIzOTA0MXAUgAAAAAASAFVwcm9vZkBtZXRhY29kZS5iaXpodHRwczov
L3d3dy5yZWRkaXQuY29tL3VzZXIvS3ViaWtQaXhlbC9jb21tZW50cy9pcW1yMXMv
a2V5b3hpZGVfb3BlbnBncF92ZXJpZmljYXRpb24vOxSAAAAAABIAIHByb29mQG1l
dGFjb2RlLmJpemh0dHBzOi8vY2hhb3Muc29jaWFsL0BrdWJpa3BpeGVsAAoJEAvh
qa4W9BeiXbUBAOlZ9boXw9wqYgYgaZz3vexLpK35DAoptsWEdJbIcwCZAP0QtNFq
AYmoUHEdjbgsF540e0s20jx0IKJrYlP0FMfqAcLCPwQTFggCpwIbAwUJCWYBgAUL
CQgHAgYVCgkICwIEFgIDAQIeAQIXgBYhBOiSPg+cfISmY6N/hQvhqa4W9BeiBQJf
n8aAPRSAAAAAABIAInByb29mQG1ldGFjb2RlLmJpei8vZi5mcmVpbmV0ei5jaC9w
cm9maWxlL2t1YmlrcGl4ZWzAIBSAAAAAABIAxXByb29mQG1ldGFjb2RlLmJpenht
cHA6a3ViaWtwaXhlbEBkaXNtYWlsLmRlP29tZW1vLXNpZC0xMjY2NzUwODA9ZTZl
ZjNhNTA0NGEwM2Y4MGMwYzVlODM1OWM2MjkxM2U2NDZkNmYxOTE3NTJlOGFmMDJj
MDNiZWFiNzkwY2EwMztvbWVtby1zaWQtMTQ4NTUwMzQ4NT0xYzUyNTFjOGExMDg2
Mjk4MTcwMzJjZDgwNzcxMzI0YzU3MmIzZDZhMzlmM2RkYzZkZGYyY2Y0ZTBlYjdj
YTdhRhSAAAAAABIAK3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vY29kZWJlcmcu
b3JnL0t1YmlrUGl4ZWwvZ2l0ZWFfcHJvb2ZUFIAAAAAAEgA5cHJvb2ZAbWV0YWNv
ZGUuYml6aHR0cHM6Ly90d2l0dGVyLmNvbS9rdWJpa3BpeGVsL3N0YXR1cy8xMzA0
MDA4MDMwMDQ5MjM5MDQxcBSAAAAAABIAVXByb29mQG1ldGFjb2RlLmJpemh0dHBz
Oi8vd3d3LnJlZGRpdC5jb20vdXNlci9LdWJpa1BpeGVsL2NvbW1lbnRzL2lxbXIx
cy9rZXlveGlkZV9vcGVucGdwX3ZlcmlmaWNhdGlvbi87FIAAAAAAEgAgcHJvb2ZA
bWV0YWNvZGUuYml6aHR0cHM6Ly9jaGFvcy5zb2NpYWwvQGt1YmlrcGl4ZWwACgkQ
C+Gprhb0F6K2OgD9GXduG/ulBkQVdgTad7++0rV3Z6aW9Gly4lYLtNGyMg8BAKZ+
D0nY2RR+hUp/000RFCnALFntE0OyvP26CDfiKCUFwsJCBBMWCAKqAhsDBQkJZgGA
BQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAFiEE6JI+D5x8hKZjo3+FC+Gprhb0F6IF
Al+C7DQ9FIAAAAAAEgAicHJvb2ZAbWV0YWNvZGUuYml6Ly9mLmZyZWluZXR6LmNo
L3Byb2ZpbGUva3ViaWtwaXhlbMAgFIAAAAAAEgDFcHJvb2ZAbWV0YWNvZGUuYml6
eG1wcDprdWJpa3BpeGVsQGRpc21haWwuZGU/b21lbW8tc2lkLTEyNjY3NTA4MD1l
NmVmM2E1MDQ0YTAzZjgwYzBjNWU4MzU5YzYyOTEzZTY0NmQ2ZjE5MTc1MmU4YWYw
MmMwM2JlYWI3OTBjYTAzO29tZW1vLXNpZC0xNDg1NTAzNDg1PTFjNTI1MWM4YTEw
ODYyOTgxNzAzMmNkODA3NzEzMjRjNTcyYjNkNmEzOWYzZGRjNmRkZjJjZjRlMGVi
N2NhN2FGFIAAAAAAEgArcHJvb2ZAbWV0YWNvZGUuYml6aHR0cHM6Ly9jb2RlYmVy
Zy5vcmcvS3ViaWtQaXhlbC9naXRlYV9wcm9vZlQUgAAAAAASADlwcm9vZkBtZXRh
Y29kZS5iaXpodHRwczovL3R3aXR0ZXIuY29tL2t1YmlrcGl4ZWwvc3RhdHVzLzEz
MDQwMDgwMzAwNDkyMzkwNDE+FIAAAAAAEgAjcHJvb2ZAbWV0YWNvZGUuYml6aHR0
cHM6Ly9tYXN0b2Rvbi5zb2NpYWwvQGt1YmlrcGl4ZWxwFIAAAAAAEgBVcHJvb2ZA
bWV0YWNvZGUuYml6aHR0cHM6Ly93d3cucmVkZGl0LmNvbS91c2VyL0t1YmlrUGl4
ZWwvY29tbWVudHMvaXFtcjFzL2tleW94aWRlX29wZW5wZ3BfdmVyaWZpY2F0aW9u
LwAKCRAL4amuFvQXou4oAQDVDIiYa/hQWgbBchkUBA1OFDMZ2IcGawceclFHeQ5F
mQEAzDm95OJVs4qcg3t4w7lKk9K0x0vDm9A5Qc6xu74ZOAzCwgQEExYIAmwCGwMF
CQlmAYAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOjf4UL4amu
FvQXogUCX24EHnAUgAAAAAASAFVwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3d3
dy5yZWRkaXQuY29tL3VzZXIvS3ViaWtQaXhlbC9jb21tZW50cy9pcW1yMXMva2V5
b3hpZGVfb3BlbnBncF92ZXJpZmljYXRpb24vPhSAAAAAABIAI3Byb29mQG1ldGFj
b2RlLmJpemh0dHBzOi8vbWFzdG9kb24uc29jaWFsL0BrdWJpa3BpeGVsVBSAAAAA
ABIAOXByb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vdHdpdHRlci5jb20va3ViaWtw
aXhlbC9zdGF0dXMvMTMwNDAwODAzMDA0OTIzOTA0MUYUgAAAAAASACtwcm9vZkBt
ZXRhY29kZS5iaXpodHRwczovL2NvZGViZXJnLm9yZy9LdWJpa1BpeGVsL2dpdGVh
X3Byb29mwCAUgAAAAAASAMVwcm9vZkBtZXRhY29kZS5iaXp4bXBwOmt1YmlrcGl4
ZWxAZGlzbWFpbC5kZT9vbWVtby1zaWQtMTI2Njc1MDgwPWU2ZWYzYTUwNDRhMDNm
ODBjMGM1ZTgzNTljNjI5MTNlNjQ2ZDZmMTkxNzUyZThhZjAyYzAzYmVhYjc5MGNh
MDM7b21lbW8tc2lkLTE0ODU1MDM0ODU9MWM1MjUxYzhhMTA4NjI5ODE3MDMyY2Q4
MDc3MTMyNGM1NzJiM2Q2YTM5ZjNkZGM2ZGRmMmNmNGUwZWI3Y2E3YQAKCRAL4amu
FvQXovrQAPwOF7iP/u9Jw0sqZP36S8Onsd1DhALJZAonURVanO1h/QD/QR24N31m
iN3q511t1HJnc/NhRV4bJYF7diIj4xPeaAXCwjoEExYIAqICGwMFCQlmAYAFCwkI
BwIGFQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOjf4UL4amuFvQXogUCX24D
CMAgFIAAAAAAEgDFcHJvb2ZAbWV0YWNvZGUuYml6eG1wcDprdWJpa3BpeGVsQGRp
c21haWwuZGU/b21lbW8tc2lkLTEyNjY3NTA4MD1lNmVmM2E1MDQ0YTAzZjgwYzBj
NWU4MzU5YzYyOTEzZTY0NmQ2ZjE5MTc1MmU4YWYwMmMwM2JlYWI3OTBjYTAzO29t
ZW1vLXNpZC0xNDg1NTAzNDg1PTFjNTI1MWM4YTEwODYyOTgxNzAzMmNkODA3NzEz
MjRjNTcyYjNkNmEzOWYzZGRjNmRkZjJjZjRlMGViN2NhN2FGFIAAAAAAEgArcHJv
b2ZAbWV0YWNvZGUuYml6aHR0cHM6Ly9jb2RlYmVyZy5vcmcvS3ViaWtQaXhlbC9n
aXRlYV9wcm9vZlQUgAAAAAASADlwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3R3
aXR0ZXIuY29tL2t1YmlrcGl4ZWwvc3RhdHVzLzEzMDQwMDgwMzAwNDkyMzkwNDE+
FIAAAAAAEgAjcHJvb2ZAbWV0YWNvZGUuYml6aHR0cHM6Ly9tYXN0b2Rvbi5zb2Np
YWwvQGt1YmlrcGl4ZWw1FIAAAAAAEgAacHJvb2ZAbWV0YWNvZGUuYml6eG1wcDpr
dWJpa3BpeGVsQGRpc21haWwuZGVwFIAAAAAAEgBVcHJvb2ZAbWV0YWNvZGUuYml6
aHR0cHM6Ly93d3cucmVkZGl0LmNvbS91c2VyL0t1YmlrUGl4ZWwvY29tbWVudHMv
aXFtcjFzL2tleW94aWRlX29wZW5wZ3BfdmVyaWZpY2F0aW9uLwAKCRAL4amuFvQX
ohDiAP9MjXvYJ1goFub1HSxmPLZDB0pZbzx34vxj+22ERI+lRwEAoDxWzApLQTBP
4H5ZjsNmp8HpaCO7TVLd1w+ob3n0ygnCwVgEExYIAcACGwMFCQlmAYAFCwkIBwIG
FQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOjf4UL4amuFvQXogUCX1s36HAU
gAAAAAASAFVwcm9vZkBtZXRhY29kZS5iaXpodHRwczovL3d3dy5yZWRkaXQuY29t
L3VzZXIvS3ViaWtQaXhlbC9jb21tZW50cy9pcW1yMXMva2V5b3hpZGVfb3BlbnBn
cF92ZXJpZmljYXRpb24vNRSAAAAAABIAGnByb29mQG1ldGFjb2RlLmJpenhtcHA6
a3ViaWtwaXhlbEBkaXNtYWlsLmRlPhSAAAAAABIAI3Byb29mQG1ldGFjb2RlLmJp
emh0dHBzOi8vbWFzdG9kb24uc29jaWFsL0BrdWJpa3BpeGVsVBSAAAAAABIAOXBy
b29mQG1ldGFjb2RlLmJpemh0dHBzOi8vdHdpdHRlci5jb20va3ViaWtwaXhlbC9z
dGF0dXMvMTMwNDAwODAzMDA0OTIzOTA0MUYUgAAAAAASACtwcm9vZkBtZXRhY29k
ZS5iaXpodHRwczovL2NvZGViZXJnLm9yZy9LdWJpa1BpeGVsL2dpdGVhX3Byb29m
AAoJEAvhqa4W9BeiNCwA/ipSuXa5g12eceGvJyC5FTLxt1JubJktcOBwwL0Cha38
AP93gBNjhuyCz34A+y9abI8iabSbyKkiU2XBkZibwWvCD8LA5wQTFggBTwIbAwUJ
CWYBgAULCQgHAgYVCgkICwIEFgIDAQIeAQIXgBYhBOiSPg+cfISmY6N/hQvhqa4W
9BeiBQJfWgoiRhSAAAAAABIAK3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vY29k
ZWJlcmcub3JnL0t1YmlrUGl4ZWwvZ2l0ZWFfcHJvb2ZUFIAAAAAAEgA5cHJvb2ZA
bWV0YWNvZGUuYml6aHR0cHM6Ly90d2l0dGVyLmNvbS9rdWJpa3BpeGVsL3N0YXR1
cy8xMzA0MDA4MDMwMDQ5MjM5MDQxPhSAAAAAABIAI3Byb29mQG1ldGFjb2RlLmJp
emh0dHBzOi8vbWFzdG9kb24uc29jaWFsL0BrdWJpa3BpeGVsNRSAAAAAABIAGnBy
b29mQG1ldGFjb2RlLmJpenhtcHA6a3ViaWtwaXhlbEBkaXNtYWlsLmRlAAoJEAvh
qa4W9BeilyQBALPhB9HwQ0wVjyKhzcN2wRazC+gQdut7O/xjki9xQyvCAQDQmfhR
PSOzir/lI7XtGmOxnMUhIDAeJeTa3ZAvtzumDsLA5wQTFggBTwIbAwUJCWYBgAUL
CQgHAgYVCgkICwIEFgIDAQIeAQIXgBYhBOiSPg+cfISmY6N/hQvhqa4W9BeiBQJf
Wgi6RhSAAAAAABIAK3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vY29kZWJlcmcu
b3JnL0t1YmlrUGl4ZWwvZ2l0ZWFfcHJvb2ZUFIAAAAAAEgA5cHJvb2ZAbWV0YWNv
ZGUuYml6aHR0cHM6Ly90d2l0dGVyLmNvbS9rdWJpa3BpeGVsL3N0YXR1cy8xMzA0
MDA4MDMwMDQ5MjM5MDQxPhSAAAAAABIAI3Byb29mQG1ldGFjb2RlLmJpemh0dHBz
Oi8vbWFzdG9kb24uc29jaWFsL0BrdWJpa3BpeGVsNRSAAAAAABIAGnByb29mQG1l
dGFjb2RlLmJpenhtcHA6a3ViaWtwaXhlbEBkaXNtYWlsLmRlAAoJEAvhqa4W9Bei
yEIBAM7VSshLrz4c+1/Slx/xkQwuaRbH5d3qVNElrbDwoSxnAQDb6AhkzIRZx1RU
D0ENVaoi1+1GbAvqLgpsp7DL5fuZB8LAoAQTFggBCAIbAwUJCWYBgAULCQgHAgYV
CgkICwIEFgIDAQIeAQIXgBYhBOiSPg+cfISmY6N/hQvhqa4W9BeiBQJfWgWMNRSA
AAAAABIAGnByb29mQG1ldGFjb2RlLmJpenhtcHA6a3ViaWtwaXhlbEBkaXNtYWls
LmRlPhSAAAAAABIAI3Byb29mQG1ldGFjb2RlLmJpemh0dHBzOi8vbWFzdG9kb24u
c29jaWFsL0BrdWJpa3BpeGVsVBSAAAAAABIAOXByb29mQG1ldGFjb2RlLmJpemh0
dHBzOi8vdHdpdHRlci5jb20va3ViaWtwaXhlbC9zdGF0dXMvMTMwNDAwODAzMDA0
OTIzOTA0MQAKCRAL4amuFvQXor3xAQCA2e1hR8LIFoP6Mwu/+XOkLUKoiMZLsaWq
w/SI8qirDQD/UX8o6Ru4m7Ohhq3tQYD3haSTVnaUAAHwPuOvrT8QdQ3CwGoEExYI
ANICGwMFCQlmAYAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AWIQTokj4PnHyEpmOj
f4UL4amuFvQXogUCX1oEJlQUgAAAAAASADlwcm9vZkBtZXRhY29kZS5iaXpodHRw
czovL3R3aXR0ZXIuY29tL2t1YmlrcGl4ZWwvc3RhdHVzLzEzMDQwMDgwMzAwNDky
MzkwNDE+FIAAAAAAEgAjcHJvb2ZAbWV0YWNvZGUuYml6aHR0cHM6Ly9tYXN0b2Rv
bi5zb2NpYWwvQGt1YmlrcGl4ZWwACgkQC+Gprhb0F6IZAwEA1nbfl4zbu3YWT71F
lW1neZ7HMSqAibqJJmhLxBWDhHwA/1VohjocXazX4ha+Lj+UCt1cHaTy8vrtvpaE
WwMWMV8GwsAVBBMWCAB9AhsDBQkJZgGABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheA
FiEE6JI+D5x8hKZjo3+FC+Gprhb0F6IFAl9aAV8+FIAAAAAAEgAjcHJvb2ZAbWV0
YWNvZGUuYml6aHR0cHM6Ly9tYXN0b2Rvbi5zb2NpYWwvQGt1YmlrcGl4ZWwACgkQ
C+Gprhb0F6LwRwD+IgRbdTSeWxXziibZ1NBXhOCqTwqOu+EDjaU3rqjMgD4A/RUo
9uBKHUMdaMhLcaWn4JWDfUotPZohuql2deegu6ECwpYEExYIAD4WIQTokj4PnHyE
pmOjf4UL4amuFvQXogUCXz0j+QIbAwUJCWYBgAULCQgHAgYVCgkICwIEFgIDAQIe
AQIXgAAKCRAL4amuFvQXonUhAQCxmovmX/cno3teyEy5PA1s8srjxqXAE+iFATIt
JuSCAQD/Y3F+051YOgP6bkNIlbvCBu9CM32x1FJiwEfjlmtsLQTOOARfPSP5Egor
BgEEAZdVAQUBAQdAWtqXzyNhaiuY797OAaiOJOZiYsj2R1pvE+L4Lpv0VCsDAQgH
wn4EGBYIACYWIQTokj4PnHyEpmOjf4UL4amuFvQXogUCXz0j+QIbDAUJCWYBgAAK
CRAL4amuFvQXoo3nAP9NHPloYq01YXReZ9vNwUKvqE0rZj1dZd2PloSBjAbVAAD+
J4QGMUv3rUb5nr5BcgfoLzrPiRgHB8EHWQeqmInRjwc=
=4OW8
-----END PGP PUBLIC KEY BLOCK-----
</textarea>
          <p>
          My PGP Public Key on <a href="https://keys.openpgp.org/search?q=E8923E0F9C7C84A663A37F850BE1A9AE16F417A2" target="_new" title="Key on keys.openpgp.org">keys.openpgp.org</a> or download it <a rel="pgpkey" href="E8923E0F9C7C84A663A37F850BE1A9AE16F417A2.txt" target="_new" title="Download PGP Public Key">here</a>.
          </p>
          <div>
            <h3><i class="las la-terminal"></i> SSH Public Key</h3>
              <span class="tooltip" aria-label="Copy SSH Public Key to clipboard">
                <button id="btn-ssh" class="small primary shadowed" type="button">
                    <i class="las la-clipboard"></i>
                </button>
              </span>
          </div>
        <label for="ssh-key">SSH Key kubikpixel@dismail.de</label>
        <input type="text" class="u-key shadowed" id="ssh-key" readonly="readonly" value="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLtKAGW7yeRIx5Hzz5Whv30PMaSOgF7cTQsDWFARI47Z2va4729knmoiHu0Utht7R4ob00rzqgmT04rciAtKhjJO4DpMaSoUl5TI6zRRjDIk4kfwnGFZySiVGlOJIMD9ShvmaNN6vuUhm/h1NRQRyqrKshRKU5MLJXB4eEnx2+oVxNvYpODJNQTb0nKyxf0JapGBaehaP2vUc9VH09r3VBj00e/xjdPpeldk5TUwly+mnNI1RGI9e7CObJtyDtp1t1AAAeq5M/iay2BAuGCSNTm+kX3WObSfPvNipMWqvUY9h26EVelhAAPvvFwskmFuZt8yrQNcTYwfWUzyz1FWhL kubikpixel@dismail.de">
        </section>
        <!-- Crypto Donation -->
        <hr>
        <h2 id="cc"><i class="las la-wallet"></i> Crypto Wallets</h2>
        <div class="row">
          <section class="card shadowed">
            <div>
              <h3><i class="lab la-bitcoin"></i> Bitcoin</h3>
                <span class="tooltip bottom" aria-label="Copy BTC address to clipboard">
                  <button id="btn-btc" class="small primary shadowed" type="button">
                      <i class="las la-clipboard"></i>
                  </button>
                </span>
            </div>
            <img class="u-photo section dark" alt="BTC QRcode" src="assets/img/btc.png" title="BTC Wallet">
            <input type="text" id="btc-adr" readonly="readonly" value="bc1qrt6x9jeu9emvaczlk3u38dfz9mt7pkjrpsame9">
          </section>
          <section class="card shadowed">
            <div>
              <h3><i class="lab la-ethereum"></i> Ethereum</h3>
                <span class="tooltip bottom" aria-label="Copy ETH address to clipboard">
                  <button id="btn-eth" class="small primary shadowed" type="button">
                      <i class="las la-clipboard"></i>
                  </button>
                </span>
            </div>
            <img class="u-photo section dark" alt="ETH QRcode" src="assets/img/eth.png" title="ETH Wallet">
            <input type="text" id="eth-adr" readonly="readonly" value="0x2076B9619240944EC660B218525FE0e1DBF46c81">
          </section>
          <section class="card shadowed">
            <div>
              <h3><i class="lab la-monero"></i> Monero</h3>
                <span class="tooltip bottom" aria-label="Copy XMR address to clipboard">
                  <button id="btn-xmr" class="small primary shadowed" type="button">
                      <i class="las la-clipboard"></i>
                  </button>
                </span>
            </div>
            <img class="u-photo section dark" alt="XMR QRcode" src="assets/img/xmr.png" title="XMR Wallet">
            <input type="text" id="xmr-adr" readonly="readonly" value="442MqGSqxDnFRKFfFpPnX8UA19YSsa5LfPYZapKgrY6iRyLfywxgz3uCyJPNjrwTjm4XPo6MtZmKDEUivVXQoci99JtsVPG">
          </section>
        </div>
    </article>
		</main>
    <hr>
		<footer>
      <p>
 <!--
        <a href="https://webring.xxiivv.com/#kubikpixel" target="_blank">
          <img src="https://webring.xxiivv.com/icon.black.svg" alt="XXIIVV webring"/>
        </a> |
 -->
        <a href="https://fediring.net/previous?host=thunix.net%2f~kubikpixel">🠈</a>
        <a href="https://fediring.net/">Fediring</a>
        <a href="https://fediring.net/next?host=thunix.net%2f~kubikpixel">🠊</a>
        |
        <a href="http://geekring.net/site/165/previous">🠈</a>
        <a href="http://geekring.net/site/165/random">Geekring random</a>
        <a href="http://geekring.net/site/165/next">🠊</a>
      </p>
		 	<p>&copy; &amp; &lt;&check;&gt; with &hearts; 2022 by KubikPixel</p>
		</footer>
    <script type="text/javascript" src="assets/js/toastify.js"></script>
    <script type="text/javascript">
/* return content of input field to variable text */
var pgpfpr = document.getElementById("pgp-fpr");
var pgpkey = document.getElementById("pgp-key");
var sshkey = document.getElementById("ssh-key");
var ethadr = document.getElementById("eth-adr");
var btcadr = document.getElementById("btc-adr");
var xmradr = document.getElementById("xmr-adr");

/* return button to variable btn */
var btnfpr = document.getElementById("btn-fpr");
var btnkey = document.getElementById("btn-key");
var btnssh = document.getElementById("btn-ssh");
var btneth = document.getElementById("btn-eth");
var btnbtc = document.getElementById("btn-btc");
var btnxmr = document.getElementById("btn-xmr");

/* call function on button click */
btnfpr.onclick = function() {
  pgpfpr.select();
  document.execCommand("copy");
  Toastify({
    text: "PGP Fingerprint was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
btnkey.onclick = function() {
  pgpkey.select();
  document.execCommand("copy");
  Toastify({
    text: "PGP Public Key was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
btnssh.onclick = function() {
  sshkey.select();
  document.execCommand("copy");
  Toastify({
    text: "SSH Public Key was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
btneth.onclick = function() {
  ethadr.select();
  document.execCommand("copy");
  Toastify({
    text: "Ethereum address was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
btnbtc.onclick = function() {
  btcadr.select();
  document.execCommand("copy");
  Toastify({
    text: "Bitcoin address was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
btnxmr.onclick = function() {
  xmradr.select();
  document.execCommand("copy");
  Toastify({
    text: "Monero address was successfully copied to your clipboard.",
    backgroundColor: "linear-gradient(to right, #00b09b, #06c09d)",
    duration: 4500,
    stopOnFocus: true,
    close: true,
  }).showToast();
}
  </script>
  </body>
</html>
<!-- Try this in our terminal: "curl -sL thunix.net/~kubikpixel"
[2J[m
[36m╔═════════════════════════════════════════════════════════╗
[36m║[m          [31m__ __     __   _ __    ___  _          __      [36m║
[36m║[m         [31m/ //_/_ __/ /  (_) /__ / _ \(_)_ _____ / /[m      [36m║
[36m║[m        [31m/ ,< / // / _ \/ /  '_// ___/ /\ \ / -_) /[m       [36m║
[36m║[m       [31m/_/|_|\_,_/_.__/_/_/\_\/_/  /_//_\_\\__/_/[m        [36m║
[36m║[m          [31m-~=# 161 = floss = cypher = nrk #=~-[m           [36m║
[36m║                                                         ║
[36m╠═════════════════════════════════════════════════════════╣
[36m║                                                         ║
[36m║[m [31m■[m [4m[1mKubikPixel:[m  [1mWeb & App developer with 3D experience[m [31m■[m [36m║
[36m║                                                         ║
[36m║[m        [4m[1mEmail:[m  [32mkubikpixel@dismail.de                    [36m║
[36m║[m      [4m[1mPGP FPR:[m  [2mE892 3E0F 9C7C 84A6 63A3                 [36m║
[36m║[m                [2m7F85 0BE1 A9AE 16F4 17A2                 [36m║
[36m║[m         [4m[1mXMPP:[m  [33mkubikpixel@5222.de                       [36m║
[36m║[m       [4m[1mMatrix:[m  [36m@kubikpixel:tchncs.de                    [36m║
[36m║                                                         ║
[36m║[m          [4m[1mWeb:[m  [2mhttps://thunix.net/[m[31m~kubikpixel/          [36m║
[36m║[m         [4m[1mBlog:[m  [2mhttps://paper.wf/[m[32mkubikpixel/             [36m║
[36m║[m       [4m[1mGemini:[m  [2mgemini://tilde.pink/[m[37m~kubikpixel/         [36m║
[36m║                                                         ║
[36m║[m     [4m[1mMastodon:[m  [2mhttps://chaos.social/[m[34m@kubikpixel         [36m║
[36m║[m       [4m[1mReddit:[m  [2mhttps://www.reddit.com/user/[m[31mKubikPixel   [36m║
[36m║[m     [4m[1mGit repo:[m  [2mhttps://codeberg.org/[m[36mKubikPixel          [36m║
[36m║                                                         ║
[36m║[m         [4m[1mCard:[m  [33mcurl -sL 0x0.st/NlpO                     [36m║
[36m║                                                         ║
[36m╚═════════════════════════════════════════════════════════╝[m

-->
